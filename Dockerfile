FROM python:3.7

RUN apt-get update \
    && apt-get install -y --no-install-recommends gosu \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

ARG COMMIT_SHA
ENV COMMIT_SHA ${COMMIT_SHA}
ARG COMMIT_REF_SLUG
ENV COMMIT_REF_SLUG ${COMMIT_REF_SLUG}

COPY . /usr/src/app

ENTRYPOINT ["python3", "main.py"]