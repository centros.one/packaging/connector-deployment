#!/usr/bin/env bash
set -euo pipefail

todo() {
  echo "TODO"
}

find-cluster-name() {
  # extract cluster name and (optionally) namespace from connect url
  if [[ $CONNECT_URL =~ ^https?://([[:alnum:].-]*$CLUSTER_NAME_SUFFIX)(\.([[:alnum:]-]*))? ]]; then
    cluster_name=${BASH_REMATCH[1]}
    cluster_namespace=${BASH_REMATCH[3]}
    if [[ -z $cluster_namespace ]]; then
      # when a namespace is not part of the connect url, the cluster must be in the target namespace
      cluster_namespace=$NAMESPACE
    fi

    # look for a connect instance with matching name+namespace
    instances=($(kubectl get deploy -n "$cluster_namespace" -l=app="$CONNECT_K8S_APP" \
      -o=jsonpath="{.items[*].metadata.name}"))

    for (( n = 0; n < ${#instances[@]}; n++ )); do
      # check if the cluster name matches the name extracted from the connect url
      if [[ ${instances[n]} == $cluster_name ]]; then
        # yes -> this is the cluster the connect url belongs to
        return 0
      fi
    done
  fi

  echo "Unable to find kafka cluster for bootstrap-server $CONNECT_URL"
  exit 1
}

wait-for-cluster-ready() {
  sleeptime=10
  if [[ $CLUSTER_READY_TIMEOUT > 0 ]]; then
    retries=$((CLUSTER_READY_TIMEOUT/sleeptime))
  else
    retries=-1
  fi
 
  find-cluster-name
  echo "Connect cluster name: $cluster_name / namespace: $cluster_namespace"

  while : ; do
    status=($(kubectl get deploy --request-timeout="${KUBECTL_TIMEOUT}s" -n "$cluster_namespace" "$cluster_name" \
      -o=jsonpath='{.status.readyReplicas}{"\t"}{.status.replicas}'))

    ready=${status[0]}
    repl=${status[1]}
    echo "$ready/$repl nodes on cluster '$cluster_name' ready, tries left: $([[ $retries > 0 ]] && echo $retries || echo '∞')"
    if [[ $ready == $repl ]] || [[ $retries == 0 ]]; then
      break # done
    fi
    sleep $sleeptime
    if [[ $retries > 0 ]]; then
      ((retries--))
    fi
  done

  if [[ $retries == 0 ]]; then
    echo "Error: cluster not ready, retries exhausted"
    exit 1
  fi
}

prepare-manifest() {
  export CONNECTOR_NAME=$1
  export IMAGE_REPOSITORY=$2
  export IMAGE_DIGEST=$3
  # other values for the template are already set as env vars by porter from the parameters
  gomplate -f k8s/job-template.yaml -o k8s/job.yaml
  if [[ $VERBOSE == "true" ]]; then
    cat k8s/job.yaml
  fi
}

wait-for-job-complete() {
  connector_name=$1
  all=($(kubectl get jobs --request-timeout="${KUBECTL_TIMEOUT}s" -n "$NAMESPACE" -l=k8s-app=connector-deployment -l=connector-name="$connector_name" \
    -o=jsonpath="{.items[*].metadata.name}"))

  if [[ ${#all[@]} < 1 ]]; then
    echo "Error: job not found"
    exit 1
  fi

  sleeptime=2
  if [[ $JOB_COMPLETE_TIMEOUT > 0 ]]; then
    retries=$((JOB_COMPLETE_TIMEOUT/sleeptime))
  else
    retries=-1
  fi

  while : ; do
    active=($(kubectl get jobs --request-timeout="${KUBECTL_TIMEOUT}s" -n "$NAMESPACE" -l=k8s-app=connector-deployment -l=connector-name="$connector_name" \
      -o=jsonpath="{.items[?(@.status.active==1)].metadata.name}"))

    echo "Waiting for job to complete, tries left: $([[ $retries > 0 ]] && echo $retries || echo '∞')"
    if [[ ${#active[@]} < 1 ]] || [[ $retries == 0 ]]; then
      break # done
    fi
    sleep $sleeptime
    if [[ $retries > 0 ]]; then
      ((retries--))
    fi
  done

  if [[ $retries == 0 ]]; then
    echo "Error: job did not complete, retries exhausted"
    exit 1
  fi

  # TODO: check job success/failure state & output log tail in case of failure
}

clean-jobs() {
  connector_name=$1
  all=$2
  extra_args=()
  if [[ $all != true ]]; then
    extra_args+=("--field-selector" "status.successful=1")
  fi
  kubectl delete jobs --request-timeout="${KUBECTL_TIMEOUT}s" -n "$NAMESPACE" -l=k8s-app=connector-deployment -l=connector-name="$connector_name" "${extra_args[@]}"
}

check-job-status() {
  connector_name=$1
  # successful jobs have been cleaned up; if any remain, they must have failed
  failed=($(kubectl get jobs --request-timeout="${KUBECTL_TIMEOUT}s" -n "$NAMESPACE" -l=k8s-app=connector-deployment -l=connector-name="$connector_name" \
      -o=jsonpath="{.items[*].metadata.name}"))
  if [[ ${#failed[@]} > 0 ]]; then
    echo "Error: job has failed"
    exit 1
  fi
}

# Call the requested function and pass the arguments as-is
"$@"
