import sys
import os
import json
import requests
import urllib.parse

base_url = os.environ.get('CONNECT_URL')
if base_url is None or not base_url or base_url.isspace():
    print("ERROR: required environment variable CONNECT_URL is not set")
    sys.exit(1)

connector_name = os.environ.get('CONNECTOR_NAME')
if connector_name is None or not connector_name or connector_name.isspace():
    print("ERROR: required environment variable CONNECTOR_NAME is not set")
    sys.exit(1)

connector_config = os.environ.get('CONNECTOR_CONFIG')
if connector_config is None or not connector_config or connector_config.isspace():
    print("ERROR: required environment variable CONNECTOR_CONFIG is not set")
    sys.exit(1)

try:
    config_json = json.loads(connector_config)
except Exception as ex:
    print("ERROR: Cannot parse connector config, reason: " + str(ex))
    sys.exit(1)

try:
    response = requests.put(urllib.parse.urljoin(base_url, "connectors/" + connector_name + "/config"),
                            json=config_json)
    response_json = json.loads(response.content.decode('utf-8'))

    if response.status_code == 200:
        print("Connector already exists; configuration updated")
    elif response.status_code == 201:
        print("Connector created")
    else:
        print("ERROR: Response status code " + str(response.status_code) + "; message from API: " + response_json.get(
            'message'))
        sys.exit(1)
except Exception as ex:
    print("ERROR: " + str(ex))
    sys.exit(1)
